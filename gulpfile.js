var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var runSequence = require('run-sequence');
var pkg = require('./package.json');
var dirs = pkg['h5bp-configs'].directories;
var plugins = require('gulp-load-plugins')();


var vendor_js_files = [
    './bower_components/jquery/dist/jquery.js',
    './bower_components/angular/angular.js',
    './bower_components/bootstrap/dist/js/bootstrap.js'
];

var app_js_files = [
    './src/js/tree.js',
    './ng/js/controllers/main.js'
]

var css_files = [
    './bower_components/bootstrap/dist/css/bootstrap.css',
    './bower_components/bootstrap/dist/css/bootstrap-theme.css',
    './src/css/fonts.css',
    './src/css/styles.css'
];

gulp.task('minify', function(){
    gulp.src(vendor_js_files)
        .pipe(uglify())
        .pipe(concat('vendor.bundle.min.js'))
        .pipe(gulp.dest('./public/js'));

    gulp.src(app_js_files)
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('./public/js'));

    gulp.src(css_files)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./public/css'));
});


gulp.task('watch', function(){
    gulp.watch(vendor_js_files, ['minify']);
    gulp.watch(app_js_files, ['minify']);
    gulp.watch(css_files, ['minify']);
});


