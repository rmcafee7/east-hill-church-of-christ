angular.module('app').factory('SermonService', function($http){
    return {
        getSermons: function(){
            return $http.get('/api/sermons.php').then(function(response){
                return response.data;
            })
        }
    }
});
