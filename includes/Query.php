<?php

require('Database.php');

class Query
{
    public static function getAll( $table ){

        $db = new Database();
        $db->createDatabase();

        if(is_string($table)){
            $query = "SELECT * FROM " . $table;
            $data = R::getAssocRow($query);
            $db->closeDatabase();
            return $data;
        } else {
            $db->closeDatabase();
            return false;
        }

    }

}