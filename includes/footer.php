<?php

    class Footer {

        public static function renderFooter(){
            echo "
                </div>
                </div>
                <footer class='footer'>
                    <div class='container text-center'>
                        <div class='white footer-text'>
                            <p>East Hill Church of Christ</p>
                            <p>P.O. Box 10785</p>
                            <p>2078 East Nine Mile Road</p>
                            <p>Pensacola, Florida 32534</p>
                            <p>(850) 479-2130</p>
                            <a href='contact.php'>Contact Us</a>
                        </div>

                    </div>
                </footer>
              </body>
            </html>";
        }
    }