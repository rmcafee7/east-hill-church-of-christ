<?php

class Header {

    public static function setJS($js){
        echo "<script src='../public/js/vendor.bundle.min.js'></script>";
        echo "<script src='../ng/js/module/app.js'></script>";
        echo "<script src='../ng/js/services/api.js'></script>";
        echo "<script src='../ng/js/controllers/main.js'></script>";



        if(!empty($js)){
            echo $js;
        }
    }

    public static function setCSS(){
        echo "<link rel='stylesheet' href='../public/css/styles.css'>";
    }

    public static function setImages(){

    }

    public static function setHeader($js, $css, $images){
        echo "
        <html ng-app='app'>
        <head>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>East Hill Church of Christ</title>";
        Header::setCSS($css);
        Header::setJS($js);
        Header::setImages($images);
        echo "</head>";

    }

    public static function startRenderingBody(){

        echo "
          <body>
          <div id='wrap'>
          <div class='container'>
        ";

    }

    public static function renderNavigationBar(){

        echo "
        <nav class='navbar navbar-default'>
          <div class='container-fluid'>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class='navbar-header'>
              <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
                <span class='sr-only'>Toggle navigation</span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
              </button>
              <a class='navbar-brand' href='#'>East Hill Church of Christ</a>
              <div class='subheading'>
                <span>Pensacola, Florida</span>
              </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>

            <form class='navbar-form' role='search'>
                <div class='form-group'>
                  <input type='text' class='form-control' placeholder='Search'>
                  <button type='submit' class='btn btn-success'>Search</button>
                </div>

              </form>

            <div class='center-nav'>
                <ul class='nav navbar-nav'>
                <li><a href='index.php'>Home</a></li>
                <li><a href='general.php'>General</a></li>
                <li><a href='directions.php'>Directions</a></li>
                <li><a href='topics.php'>Topics</a></li>
                <li><a href='onlinecourses.php'>Online Courses</a></li>
                <li><a href='downloads.php'>Downloads</a></li>
                <li><a href='directory.php'>Directory</a></li>
                <li><a href='map.php'>Map</a></li>
              </ul>
            </div>


            </div>
          </div>
        </nav>";



    }

    public static function renderSlideShow($options){

        if(!empty($options)){

            echo "
            <div id='myCarousel' class='carousel slide'>
      <div class='carousel-inner'>";

            foreach($options as $item){

                if($item['active'] == true){
                    echo "<div class='item active'>";
                } else {
                    echo "<div class='item'>";
                }

                if(empty($item['img'])){
                    echo "<img src='../public/images/Default.jpg' alt=''>";
                }else {
                    echo "<img src='{$item['img']}' alt=''>";
                }

          echo "<div class='container'>";
            echo "<div class='carousel-caption'>";
              echo "<h1>{$item['headline']}</h1>";
              echo "<p class='lead'>{$item['body']}</p>";
              echo "<a class='btn btn-large btn-primary' href='{$item['link']}'>{$item['button_text']}</a>";
            echo "</div>";
          echo "</div>";
        echo "</div>";
            }
            echo "


      <a class='left carousel-control' href='#myCarousel' data-slide='prev'><</a>
      <a class='right carousel-control' href='#myCarousel' data-slide='next'>></a>

    </div>


        ";
        }

    }

}



?>