<?php

require('rb.php');
require('config.inc.php');


class Database
{
        public $conn;

        public function createDatabase(){

            $dsn = "mysql:host=".Config::$db_host.";dbname=".Config::$db_name;
            R::setup($dsn, Config::$db_user, Config::$db_pass);

        }

        public static function closeDatabase(){

            R::close();

        }




}

