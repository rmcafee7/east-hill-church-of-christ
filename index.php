<?php

require_once("includes/header.php");
require_once("includes/footer.php");


Header::setHeader();
Header::startRenderingBody();
Header::renderNavigationBar();

$options = array(
    'item1' => array(
        'active' => true,
        'img' => '',
        'headline' => "Meeting Times",
        'body' => "We would love to have you!",
        'link' => 'general.php',
        'button_text' => 'See our meeting times'
    ),
    'item2' => array(
        'active' => false,
        'img' => '',
        'headline' => "Learn",
        'body' => "Learn more about the work of the lord observed at East Hill",
        'link' => 'topics.php',
        'button_text' => 'Learn More'
    )
);

Header::renderSlideShow($options);


include('templates/index.html');


Footer::renderFooter();