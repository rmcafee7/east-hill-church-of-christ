<?php


require_once("includes/header.php");
require_once("includes/footer.php");


Header::setHeader();
Header::startRenderingBody();
Header::renderNavigationBar();

Footer::renderFooter();

?>