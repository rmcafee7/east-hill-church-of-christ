<?php


require_once("includes/header.php");
require_once("includes/footer.php");


Header::setHeader();
Header::startRenderingBody();
Header::renderNavigationBar();

include('templates/directions.html');

Footer::renderFooter();



?>