<?php

require_once("includes/header.php");
require_once("includes/footer.php");


Header::setHeader(null, null, null);
Header::startRenderingBody();
Header::renderNavigationBar();


include('templates/sermons.php');


Footer::renderFooter();