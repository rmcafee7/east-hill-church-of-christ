<?php

require("../includes/base.inc.php");

\Slim\Slim::registerAutoloader();


$app = new \Slim\Slim();

$app->get('/', function(){

    $sermons = Query::getAll('sermons');

    echo json_encode($sermons);

});


$app->run();

