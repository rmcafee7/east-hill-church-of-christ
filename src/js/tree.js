function popupWin(verse) {
    if (verse == "matt1513")
    {
        text =  "<html>\n<head>\n<title>Matthew 15:13</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Matthew 15:13<p>";
        text += "But he answered and said, <font color=red>&quot;Every plant which My heavenly Father has not planted ";
        text += "will be uprooted.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "matt721")
    {
        text =  "<html>\n<head>\n<title>Matthew 7:21</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Matthew 7:21<p>";
        text += "<font color=red>&quot;Not everyone who says to Me; &#146;Lord, Lord&#146; shall enter the kingdom of heaven,";
        text += " but he who does the will of My Father in heaven.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts242")
    {
        text =  "<html>\n<head>\n<title>Acts 2:42</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 2:42<p>";
        text += "And they continued steadfastly in the apostles&#146; doctrine and fellowship, in the breaking of bread,";
        text += " and in prayers.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "icor1123")
    {
        text =  "<html>\n<head>\n<title>I Corinthians 11:23-30</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Corinthians 11:23-30<p>";
        text += "For I received from the Lord that which I also delivered unto you: that the Lord Jesus on the same night ";
        text += "in which he was betrayed took bread; and when He had given thanks, he broke it and said, <font color=red>";
        text += "&quot;Take, eat; this is My body which is broken for you; do this in remembrance of Me.&quot;</font>  In ";
        text += "the same manner He also took the cup after supper, saying, <font color=red>&quot;This cup is the new ";
        text += "covenant in My blood.  This do, as often as you drink it, in remembrance of Me.&quot;<p></font>";
        text += "For as often as you eat this bread and drink this cup, you proclaim the Lord's death till he comes.<p>";
        text += "Therefore, whoever eats this bread or drinks this cup of the Lord in an unworthy manner will be guilty of ";
        text += "the body and blood of the Lord.  But let a man examine himself, and so let him eat of the bread and drink ";
        text += "of the cup.  For he who eats and drinks in an unworthy manner eats and drinks judgment to himself, not ";
        text += "discerning the Lords body.  For this reason many are weak and sick among you, and many sleep.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "lk221")
    {
        text =  "<html>\n<head>\n<title>Luke 22:1, 18-20</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Luke 22:1, 18-20<p>";
        text += "Now the Feast of Unleavened Bread drew near, which is called Passover.<p>";
        text += "<font color=red>&quot;For I say to you, I will not drink of the fruit of the vine until the kingdom of ";
        text += "God comes.&quot;</font>  And He took bread, gave thanks and broke it, and gave it to them, saying, ";
        text += "<font color=red>&quot;This is My body which is given for you; do this in remembrance of Me.&quot;";
        text += "</font>  Likewise He also took the cup after supper, saying, <font color=red>&quot;This cup is the new ";
        text += "covenant in My blood, which is shed for you.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts207")
    {
        text =  "<html>\n<head>\n<title>Acts 20:7</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 20:7<p>";
        text += "Now on the first day of the week, when the disciples came together to break bread, Paul, ready to ";
        text += "depart the next day, spoke to them and continued his message until midnight.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "col316")
    {
        text =  "<html>\n<head>\n<title>Colossians 3:16</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Colossians 3:16<p>";
        text += "Let the word of Christ dwell in you richly in all wisdom, teaching and admonishing one another in ";
        text += "psalms and hymns and spiritual songs, singing with grace in your hearts to the Lord.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "eph519")
    {
        text =  "<html>\n<head>\n<title>Ephesians 5:19</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Ephesians 5:19<p>";
        text += "Speaking to one another in psalms and hymns and spiritual songs, singing and making melody in your ";
        text += "hearts to the Lord.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "icor162")
    {
        text =  "<html>\n<head>\n<title>I Corinthians 16:2</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Corinthians 16:2<p>";
        text += "On the first day of the week let each one of you lay something aside, storing up as he may prosper, ";
        text += "that there be no collections when I come.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "matt157")
    {
        text =  "<html>\n<head>\n<title>Matthew 15:7-9</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Matthew 15:7-9<p>";
        text += "<font color=red>&quot;Hypocrites!  Well did Isaiah prophesy about you, saying:<br>";
        text += "<dir>&#146;These people draw near to Me with their mouth,<br>";
        text += "And honor Me with their lips,<br>";
        text += "But their heart is far from Me.<br>";
        text += "And in vain they worship Me,<br>";
        text += "Teaching as doctrines the commandments of men.&#146;&quot;</dir>\n</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "eph412")
    {
        text =  "<html>\n<head>\n<title>Ephesians 4:12</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Ephesians 4:12<p>";
        text += "For the equipping of the saints for the work of the ministry, for the edifying of the body of Christ.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "eph416")
    {
        text =  "<html>\n<head>\n<title>Ephesians 4:16</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Ephesians 4:16<p>";
        text += "From whom the whole body, joined and knit together by what every joint supplies, according to ";
        text += "the effective working by which every part does its share, causes growth of the body for the ";
        text += "edifying of itself in love.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts61")
    {
        text =  "<html>\n<head>\n<title>Acts 6:1-6</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 6:1-6<p>";
        text += "Now in those days, when the number of the disciples was multiplying, there arose a complaint against ";
        text += "the Hebrews by the Hellenists, because their widows were neglected in the daily distribution.  Then ";
        text += "the twelve summoned the multitude of the disciples and said, &quot;It is not desirable that we should ";
        text += "leave the word of God and serve tables.  Therefore, brethren, seek out from among you seven men of good ";
        text += "reputation, full of the Holly Spirit and wisdom, whom we may appoint over this business; but we will give ";
        text += "ourselves continually to prayer and to the ministry of the word.&quot;";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "itim315")
    {
        text =  "<html>\n<head>\n<title>I Timothy 3:15</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Timothy 3:15<p>";
        text += "But if I am delayed, I write so that you may know how you ought to conduct yourself in the house of God,";
        text += " which is the church of the living God, the pillar and ground of the truth.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "rom1017")
    {
        text =  "<html>\n<head>\n<title>Romans 10:17</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Romans 10:17<p>";
        text += "So then faith comes by hearing, and hearing by the word of God.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "mk1616")
    {
        text =  "<html>\n<head>\n<title>Mark 16:16</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Mark 16:16<p>";
        text += "He who believes and is baptized will be saved; but he who does not believe will be condemned.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts238")
    {
        text =  "<html>\n<head>\n<title>Acts 2:38</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 2:38<p>";
        text += "Then Peter said unto them, &quot;Repent, and let every one of you be baptized in the name of ";
        text += "Jesus Christ for the remission of sins; and you shall receive the gift of the Holy Spirit.&quot;";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "rom109")
    {
        text =  "<html>\n<head>\n<title>Romans 10:9-10</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Romans 10:9-10<p>";
        text += "That if you confess with your mouth the Lord Jesus and believe in your heart that God has raised ";
        text += "him from the dead, you will be saved.  For with the heart one believes unto righteousness, and with ";
        text += "the mouth confession is made unto salvation.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "gal327")
    {
        text =  "<html>\n<head>\n<title>Galatians 3:27</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Galatians 3:27<p>";
        text += "For as many of you as were baptized into Christ have put on Christ.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "gal67")
    {
        text =  "<html>\n<head>\n<title>Galatians 6:7</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Galatians 6:7<p>";
        text += "Do not be deceived, God is not mocked; for whatever a man sows, that he will also reap.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "gen111")
    {
        text =  "<html>\n<head>\n<title>Genesis 1:11-12</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Genesis 1:11-12<p>";
        text += "Then God said, &quot;Let the earth bring forth grass, the herb that yields seed, and the fruit ";
        text += "tree that yields fruit according to its kind, whose seed is in itself, on the earth&quot;; and it ";
        text += "was so.  And the earth brought forth grass, the herb that yields seed according to its kind, and the tree ";
        text += "yields fruit, whose seed is in itself according to its kind.  And God saw that it was good.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "matt1618")
    {
        text =  "<html>\n<head>\n<title>Matthew 16:18</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Matthew 16:18<p>";
        text += "<font color=red>&quot;And I also say to you that you are Peter, and on this rock I will build My church,";
        text += " and the gates of Hades shall not prevail against it.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "phil11")
    {
        text =  "<html>\n<head>\n<title>Philippians 1:1</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Philippians 1:1<p>";
        text += "Paul and Timothy, bondservants of Jesus Christ,<br>";
        text += "To all the saints in Christ Jesus who are at Philippi, with the bishops and deacons:";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "itim38")
    {
        text =  "<html>\n<head>\n<title>I Timothy 3:8-13</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Timothy 3:8-13<p>";
        text += "Likewise deacons must be reverent, not double-tongued, not given to much wine, not greedy for money, ";
        text += "holding the mystery of the faith with a pure conscience.  But let these also first be tested; then let ";
        text += "them serve as deacons, being found blameless.  Likewise, their wives must be reverent, not slanderers, ";
        text += "temperate, faithful in all things.  Let deacons be the husbands of one wife, ruling their children and ";
        text += "their own houses well.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "itim31")
    {
        text =  "<html>\n<head>\n<title>I Timothy 3:1-7</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Timothy 3:1-7<p>";
        text += "This is a faithful saying: If a man desires the position of a bishop, he desires a good work.  ";
        text += "A bishop must be blameless, the husband of one wife, temperate, sober-minded, of good behavior, ";
        text += "hospitable, able to teach; not given to wine, not violent, not greedy for money, but gentle, not ";
        text += "quarrelsome, not covetous; one who rules his own house well, having his children in submission with ";
        text += "all reverence (for if a man does not know how to rule his own house, how will he take care of the church ";
        text += "of God?); not a novice, lest being puffed up with pride he fall into the same condemnation as the devil.";
        text += "  Moreover he must have a good testimony among those who are outside, lest he fall into reproach and ";
        text += "the snare of the devil.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts1423")
    {
        text =  "<html>\n<head>\n<title>Acts 14:23</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 14:23<p>";
        text += "So when they had appointed elders in every church, and prayed with fasting, they commended them to ";
        text += "the Lord in whom they had believed.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts2017")
    {
        text =  "<html>\n<head>\n<title>Acts 20:17, 28</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 20:17, 28<p>";
        text += "From Miletus he sent to Ephesus and called for the elders of the church.<br>";
        text += "Therefore take heed to yourselves and to all the flock, among which the Holy Spirit has made you ";
        text += "overseers, to shepherd the church of God which he purchased with his own blood.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "ipet51")
    {
        text =  "<html>\n<head>\n<title>I Peter 5:1-4</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Peter 5:1-4<p>";
        text += "The elders who are among you I exhort, I who am a fellow elder and a witness of the sufferings of Christ,";
        text += " and also a partaker of the glory that will be revealed:  Shepherd the flock of God that is among you, ";
        text += "serving as overseers, not by compulsion but willingly, not for dishonest gain but eagerly, nor as ";
        text += "being lords over those entrusted to you, but being examples to the flock; and when the Chief Shepherd ";
        text += "appears, you will receive the crown of glory that does not fade away.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "matt2818")
    {
        text =  "<html>\n<head>\n<title>Matt 28:18</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Matt 28:18<p>";
        text += "And Jesus came and spoke to them, saying, <font color=red>&quot;All authority has been given to Me ";
        text += "in heaven and on earth.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts1126")
    {
        text =  "<html>\n<head>\n<title>Acts 11:26</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 11:26<p>";
        text += "And when he had found him, he brought him to Antioch.  So it was for a whole year they assembled ";
        text += "with the church and taught a great many people.  And the disciples were first called Christians in Antioch.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "icor12")
    {
        text =  "<html>\n<head>\n<title>I Corinthians 1:2</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Corinthians 1:2<p>";
        text += "To the church of God which is at Corinth, to those who are sanctified in Christ Jesus, called to be ";
        text += "saints, with all who in every place call on the name of Jesus Christ our Lord, both theirs and ours:";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "jno831")
    {
        text =  "<html>\n<head>\n<title>John 8:31</title>\n</head>\n<body>\n<center>\n<br>";
        text += "John 8:31<p>";
        text += "Then Jesus said to those Jews who believed Him, <font color=red>&quot;If you abide in My word, ";
        text += "you are My disciples indeed.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "matt238")
    {
        text =  "<html>\n<head>\n<title>Matthew 23:8</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Matthew 23:8<p>";
        text += "<font color=red>&quot;But you, do not be called Rabbi; for One is your Teacher, the Christ, ";
        text += "and you are all brethren.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "acts412")
    {
        text =  "<html>\n<head>\n<title>Acts 4:12</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Acts 4:12<p>";
        text += "Nor is there salvation in any other, for there is no other name under heaven given among men by ";
        text += "which we must be saved.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "eph121")
    {
        text =  "<html>\n<head>\n<title>Ephesians 1:21-22</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Ephesians 1:21-22<p>";
        text += "Far above all principality and power and might and dominion, and every name that is named, not ";
        text += "only in this age but also in that which is to come.<br>";
        text += "And He put all things under His feet, and gave Him to be head over all things to the church.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "eph221")
    {
        text =  "<html>\n<head>\n<title>Ephesians 2:21</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Ephesians 2:21<p>";
        text += "In whom the whole building, being fitted together, grows into a holy temple in the Lord.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "col113")
    {
        text =  "<html>\n<head>\n<title>Colossians 1:13</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Colossians 1:13<p>";
        text += "He has delivered us from the power of darkness and conveyed us into the kingdom of the Son of His love.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "rom1616")
    {
        text =  "<html>\n<head>\n<title>Romans 16:16</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Romans 16:16<p>";
        text += "Greet on another with a holy kiss.  The churches of Christ greet you.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "gal16")
    {
        text =  "<html>\n<head>\n<title>Matthew 7:21</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Galatians 1:6-8<p>";
        text += "I marvel that you are turning away so soon from Him who called you in the grace of Christ, to a ";
        text += "different gospel, which is not another, but there are some who trouble you and want to pervert the ";
        text += "gospel of Christ.  But even if we, or an angel from heaven preach any other gospel to you than what ";
        text += "we have preached to you, let him be accursed.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "iitim316")
    {
        text =  "<html>\n<head>\n<title>II Timothy 3:16-17</title>\n</head>\n<body>\n<center>\n<br>";
        text += "II Timothy 3:16-17<p>";
        text += "All scripture is given by inspiration of God, and is profitable for doctrine, for reproof, for ";
        text += "correction, for instruction in righteousness, that the man of God might be complete, thoroughly ";
        text += "equipped for every good work.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "rom116")
    {
        text =  "<html>\n<head>\n<title>Romans 1:16</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Romans 1:16<p>";
        text += "For I am not ashamed of the gospel of Christ, for it is the power of God to salvation for ";
        text += "everyone who believes, for the Jew first and also for the Greek.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "iipet13")
    {
        text =  "<html>\n<head>\n<title>II Peter 1:3</title>\n</head>\n<body>\n<center>\n<br>";
        text += "II Peter 1:3<p>";
        text += "As His divine power has given to us all things that pertain to life and godliness, through the ";
        text += "knowledge of Him who called us by glory and virtue.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "jude3")
    {
        text =  "<html>\n<head>\n<title>Jude 3</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Jude 3<p>";
        text += "Beloved, while I was very diligent to write to you concerning our common salvation, I found it ";
        text += "necessary to write to you exhorting you to contend earnestly for the faith which was once for ";
        text += "all delivered to the saints.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "iijno9")
    {
        text =  "<html>\n<head>\n<title>II John 9</title>\n</head>\n<body>\n<center>\n<br>";
        text += "II John 9<p>";
        text += "Whoever transgresses and does not abide in the doctrine of Christ does not have God.  He who ";
        text += "abides in the doctrine of Christ has both the Father and the Son.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "rev2218")
    {
        text =  "<html>\n<head>\n<title>Revelations 22:18-19</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Revelations 22:18-19<p>";
        text += "For I testify to everyone who hears the words of the prophecy of this book:  If anyone adds to ";
        text += "these things, God will add to him the plagues that are written in this book; and if anyone takes ";
        text += "away from the words of the book of this prophesy, God shall take away his part from the Book of ";
        text += "Life, from the holy city, and from the things that are written in this book.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "lk811")
    {
        text =  "<html>\n<head>\n<title>Luke 8:11</title>\n</head>\n<body>\n<center>\n<br>";
        text += "Luke 8:11<p>";
        text += "<font color=red>&quot;Now the parable is this: the seed is the word of God.&quot;</font>";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }
    if (verse == "ipet122")
    {
        text =  "<html>\n<head>\n<title>I Peter 1:22-23</title>\n</head>\n<body>\n<center>\n<br>";
        text += "I Peter 1:22-23<p>";
        text += "Since you have purified your souls in obeying the truth through the Spirit in sincere love of ";
        text += "the brethren, love one another fervently with a pure heart, having been born again, not of ";
        text += "corruptible seed but of incorruptible, through the word of God which lives and abides forever.";
        text += "<p><font size=1>New King James Version</font></center>\n</body>\n</html>\n";
    }

    windowProp(text)
}

function windowProp(text) {
    newWindow = window.open('','newWin','width=500,height=500');
    newWindow.document.write(text);
}