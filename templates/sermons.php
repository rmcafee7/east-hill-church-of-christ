<?php

echo "
<div class='content'>

<div class='text-center'>
    <h3>Sermons</h3>
</div>

    <div ng-controller='SermonsController'>
    <table class='table table-bordered table-responsive'>
        <thead>
            <tr>
                <th>Sermon Title</th>
                <th>Subject</th>
                <th>Preacher</th>
                <th>Date</th>
                <th>Download Sermon</th>
            </tr>
        </thead>

        <tr ng-repeat='sermon in sermons'>
            <td>{{sermon.title}}</td>
            <td>{{sermon.category}}</td>
            <td>{{sermon.preacher}}</td>
            <td>{{sermon.date}}</td>
            <td><a href='{{sermon.audio_file_url}}'>Download</a></td>
        </tr>
    </table>

    </div>

</div>

";

?>