<?php

echo "

<div class='content text-justify'>
    <h3>Bible Study Course</h3>
        <p>This is a simple, easy-to-follow study of what the Bible teaches about God's plan of salvation for mankind.
        The course consists of 13 lessons, and you will receive personalized responses in the mail or email (your choice) when you return each lesson for grading and comments.</p>
        <br>
        <p>Click this link to download a compressed file containing the Bible Study course: <a href='../downloads/course.zip'>Bible Study Course Zip File</a> </p>
    <h3>Bible Study Material</h3>
        <p>
            These are the outlines and other supplemental material that the men of the congregation of the East Hill Church of Christ have put together to aid us in our study of God's word.
            We are using Brother Bob Waldron's material that divides the Bible into 17 periods of Bible history. Our study consists of 13 Quarters. You may download the supplementary material at your convenience.
            There are 13 .zip files which contain each lesson for each quarter.
        </p>
        <br>
        <p>Click this link to see a list of the bulletins we have online: <a href='./bible_class.php'>Bible Class</a></p>
        <br>




<div class='text-center'>
    <h3>Sermons</h3>
</div>

    <div ng-controller='SermonsController'>
    <table class='table table-bordered table-responsive'>
        <thead>
            <tr>
                <th>Sermon Title</th>
                <th>Subject</th>
                <th>Preacher</th>
                <th>Date</th>
                <th>Download Sermon</th>
            </tr>
        </thead>

        <tr ng-repeat='sermon in sermons'>
            <td>{{sermon.title}}</td>
            <td>{{sermon.category}}</td>
            <td>{{sermon.preacher}}</td>
            <td>{{sermon.date}}</td>
            <td><a href='{{sermon.audio_file_url}}'>Download</a></td>
        </tr>
    </table>

    </div>

</div>
";


?>